/* 
-------------------------------------------------------------
Clase 04 - Modulación PWM.
------------------------------------------------------------
Controlar la intensidad de un LED mediante un potenciómetro.
------------------------------------------------------------
*/

// Definición de constantes y variables:
#define pinPot A0
#define pinLed 3

int val = 0; // valor de intensidad.

void setup() {

pinMode(pinLed, OUTPUT);  
  // Saludo lumínico (FadeIn)!!
  for (val = 0; val < 256; val++) {
    analogWrite(pinLed,val); // Usa el valor de salida del for.
    delay(4); // Determina velocidad del FadeIn.
  }
}

void loop() {
  int valPot = analogRead(pinPot);
  //val = map(valorPot,0,1023,0,255); // (10 bits)
  //analogWrite(pinLed,val);
  val = valPot>>2;
  analogWrite(pinLed,val);
}
