/*
-------------------------------------
Recorrido de escala con potenciómetro
------------------------------------- 
*/

// Definición de variables
#define pinPot A0
#define pinSpk 5
#define pinBtn 2

// Arreglo con la escala octatónica:
int octScale[] = { 262, 277, 311, 330, 370, 392, 440, 466, 524 };
int tempo = 200;

void setup() {
  pinMode(pinSpk, OUTPUT);
  pinMode(pinBtn, INPUT_PULLUP);
}

void loop() {

  bool stateBtn = digitalRead(pinBtn);
  
  int valPot = analogRead(pinPot);
  valPot = map(valPot, 0, 1024, 0, 8);
  // int note = tone(pinSpk,octScale[valPot], 200);

  for (int i = 0; i <= valPot; i++) {
    if (stateBtn == LOW) {
      tempo = random(0,100);
      octScale[i] = random(100,800);
    }
    
    tone(pinSpk,octScale[i]);
    delay(tempo);
  }
}

//Problema del Bounce: El contacto tiene un "rebote": el botón puede leerse muchas veces. --> De-bounce (un filtro temporal). [Evitar que el botón se lea nuevamente luego de un intervalo de tiempo].
// Hacer un código para que Arduino reconozca tres clicks seguidos. 
