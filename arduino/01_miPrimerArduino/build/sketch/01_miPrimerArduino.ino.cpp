#include <Arduino.h>
#line 1 "/home/ibup/Documents/universidad/SISO361/arduino/01_miPrimerArduino/01_miPrimerArduino.ino"
/*
mi Primer código Arduino
Emular el clásico 'hola mundo'
de la computación física
Encender y apagar un LED
en un intervalo específico
*/

#define pinLed1 2 
#define pinLed2 3

// Intervalo de blink (ms):
int intervalo = 500; 

#line 15 "/home/ibup/Documents/universidad/SISO361/arduino/01_miPrimerArduino/01_miPrimerArduino.ino"
void setup();
#line 23 "/home/ibup/Documents/universidad/SISO361/arduino/01_miPrimerArduino/01_miPrimerArduino.ino"
void loop();
#line 15 "/home/ibup/Documents/universidad/SISO361/arduino/01_miPrimerArduino/01_miPrimerArduino.ino"
void setup() {

  // Configuraciones del Arduino:
  pinMode(pinLed1, OUTPUT);
  pinMode(pinLed2, OUTPUT);

}

void loop() {

  digitalWrite(pinLed1, HIGH); //encendemos LED
  delay(intervalo);           // esperamos intervalo (ms)
  digitalWrite(pinLed1, LOW);  // apagamos LED
  delay(intervalo);           // esperamos nuevamente

  digitalWrite(pinLed2, HIGH); //encendemos LED
  delay(intervalo);           // esperamos intervalo (ms)
  digitalWrite(pinLed2, LOW);  // apagamos LED
  delay(intervalo);           // esperamos nuevamente

}

