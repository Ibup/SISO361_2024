/*

Encender y apagar un LED
Usamos un potenciómetro
para determinar intervalo

*/

#define pinLed 2  //defino como constante el pin para conectar el LED
#define pinPot A2 //definimos pin para lectura potenciómetro

int intervalo = 500;  // definimos variable para intervalo de tiempo
// tipo entero (int) y le damos un valor inical de 500 ms

void setup() {

  // configuramos el pin LED como salida
  pinMode(pinLed, OUTPUT);

}

void loop() {

  // leer valor voltage de pin pot 
  // resultado es una valor entre 0 y 1023 (ADC de 10 bits)
  int valorPot = analogRead(pinPot);

  //opción A - mapeo directo
//  intervalo = valorPot;

  //opción B - damos una base para un intervalo mínimo
 // intervalo = valorPot + 50;

  //opción C - usamos función 'map'
  intervalo = map(valorPot,0,1023,100,400);

  digitalWrite(pinLed, HIGH);  //encendemos LED
  delay(intervalo);            // esperamos intervalo (ms)
  digitalWrite(pinLed, LOW);   // apagamos LED
  delay(intervalo);            // esperamos nuevamente
}
