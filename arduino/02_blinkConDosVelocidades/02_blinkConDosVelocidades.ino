/*

Encender y apagar un LED
Con un botón cambiar entre
dos velocidades de intervalo

*/

#define pinLed 2  //defino como constante el pin para conectar el LED
#define pinBtn 3  // pin para conectar botón pulsador

int intervalo = 500;  // definimos variable para intervalo de tiempo
// tipo entero (int) y le damos un valor inical de 500 ms

void setup() {

  // configuramos el pin LED como salida
  pinMode(pinLed, OUTPUT);

  // configuramos botón como entrada pull_up
  // revisar: https://docs.arduino.cc/learn/microcontrollers/digital-pins/
  pinMode(pinBtn, INPUT_PULLUP);
}

void loop() {

  bool estadoBtn = digitalRead(pinBtn);  //leemos estado botón (alto o bajo)

  if (estadoBtn == HIGH) {  //si el botón no está apretado
    intervalo = 400;        // velocidad lenta
  } else {
    intervalo = 100;  // velocidad rápida
  }

  digitalWrite(pinLed, HIGH);  //encendemos LED
  delay(intervalo);            // esperamos intervalo (ms)
  digitalWrite(pinLed, LOW);   // apagamos LED
  delay(intervalo);            // esperamos nuevamente
}
