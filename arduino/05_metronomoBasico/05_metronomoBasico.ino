/*
-----------------------------------------
Metrónomo Básico:
- PWM y tone() para generar el sonido.
- LED para indicar el pulso.
- Potenciómetro para modificar el tempo.
-----------------------------------------
*/

// Definición de constantes y variables.
#define pinPot A0
#define pinLed 3
#define pinSpk 5

void setup() {
  pinMode(pinLed, OUTPUT);
  pinMode(pinSpk, OUTPUT);
}

void loop() {
  // Lee el pot: se asigna a valor de tempo (ms)
  int tempo = map(analogRead(pinPot),0,1023,400,15);
  
  // Secuencia ON y OFF del metrónomo:
  digitalWrite(pinLed,HIGH);
  tone(pinSpk, 440); // tone() genera el tono en el pin Spk.
  delay(tempo); 
  digitalWrite(pinLed,LOW);
  noTone(pinSpk); // noTone da la instrucción de apagar el tono.
  delay(tempo);
}
