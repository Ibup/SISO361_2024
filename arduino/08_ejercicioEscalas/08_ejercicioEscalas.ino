/*
Ejercicio:

Programar un dispositivo que permita
recorrer una escala (mayor, menor, pentatónica, etc.)
con el potenciómetro conectado al Arduino.
El resultado debe ser escuchado a través de la función
tone()

Extra: agregar un botón para cambiar octava

*/

#define pinTon 5     // pin parlante
#define pinPot A0    // pin pot
#define durTono 250  // duración tono
#define pinBtn 8     // pin botón

int notaAnterior = 0;  // variable global de nota anterior

// Buscar frecuencias pre-definidas
// Opción A
// arreglo con escala disminuida
//float frecs[] = {234.96, 197.58, 166.14, 139.71, 117.48};

// Opción B
// usamos librería descargada (ver pestaña)
#include "pitches.h"
float frecs[] = { NOTE_A4, NOTE_C5, NOTE_D5, NOTE_F5, NOTE_G5 };

void setup() {

  // configuramos botón como entrada 'pullup'
  pinMode(pinBtn, INPUT_PULLUP);

  // saludamos!
  for (int i = 0; i < 6; i++) {
    tone(pinTon, frecs[i], 150);
    delay(150);
  }
}

void loop() {

  // leemos valor pot
  int valPot = analogRead(pinPot);
  // mapeamos pot a n° de notas (con rango reducido)
  int nota = map(valPot, 30, 1000, 0, 4);
  // leemos valor botón (invertido) y escalamos a valores de 1 y 2
  int valBtn = int(!digitalRead(pinBtn) + 1);

  if (nota != notaAnterior) {                     // si la nota es diferente a la anterior
    tone(pinTon, frecs[nota] * valBtn, durTono);  // generamos tono y escalamos según estado botón
    notaAnterior = nota;                          // actualizamos nota anterior
  }
}