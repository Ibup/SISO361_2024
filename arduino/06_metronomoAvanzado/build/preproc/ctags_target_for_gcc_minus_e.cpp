# 1 "/home/ibup/Documents/universidad/SISO361/arduino/06_metronomoAvanzado/06_metronomoAvanzado.ino"
/*
-----------------------------------------
Metrónomo Avanzado (sin función delay):
- PWM y tone() para generar el sonido.
- LED para indicar el pulso.
- Potenciómetro para modificar el tempo.
-----------------------------------------
*/

// Definición de constantes y variables.




unsigned long prevTime = millis(); // Inicialización de tiempo.
bool stateLed = 0x0; // Variable lógica de estado.

void setup() {
  pinMode(3, 0x1);
  pinMode(5, 0x1);
}

void loop() {
  // Lee el pot: se asigna a valor de tempo (ms)
  int tempo = map(analogRead(A0),0,1023,400,15);
  int dur = map(analogRead(A0),0,1023,20,100);

if ((millis() - prevTime) > tempo) {
  stateLed = !stateLed // Negación booleana.
  digitalWrite(3,stateLed);

  prevTime = millis();

  if (stateLed == 0x1) {
    tone(5, random(150,5000),dur);
    }
  else noTone(5);
  }
}
