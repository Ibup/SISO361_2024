/*
-----------------------------------------
Metrónomo Avanzado (sin función delay):
- PWM y tone() para generar el sonido.
- LED para indicar el pulso.
- Potenciómetro para modificar el tempo.
-----------------------------------------
*/

// Definición de constantes y variables.
#define pinPot A0
#define pinLed 3
#define pinSpk 5

unsigned long prevTime = millis(); // Inicialización de tiempo.
bool stateLed = LOW; // Variable lógica de estado.

void setup() {
  pinMode(pinLed, OUTPUT);
  pinMode(pinSpk, OUTPUT);
}

void loop() {
  // Lee el pot: se asigna a valor de tempo (ms)
  int tempo = map(analogRead(pinPot),0,1023,400,15);
  int dur = map(analogRead(pinPot),0,1023,20,100);

if ((millis() - prevTime) > tempo) {
  stateLed = !stateLed; // Negación booleana.
  digitalWrite(pinLed,stateLed);

  prevTime = millis();

  if (stateLed == HIGH) {
    tone(pinSpk, random(150,5000),dur);
    }
  else noTone(pinSpk);
  }
}
