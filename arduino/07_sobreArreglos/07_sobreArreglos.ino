// Aprender sobre arreglos

// creamos un arreglo con los seis
// pines dondes están conectados Leds

#define pinPot A0
int pinesLed[] = { 2, 3 };

void setup() {

  // usamos arreglo for para configurar seis pines como salida
  for (int i = 0; i < 2; i++) {
    pinMode(pinesLed[i], OUTPUT);
  }

  // saludamos!
  for (int i = 0; i < 2; i++) {
    digitalWrite(pinesLed[i], HIGH);
    delay(300);
  }

  // apagamos Leds
  for (int i = 0; i < 2; i++) {
    digitalWrite(pinesLed[i], LOW);
  }
}

void loop() {

  // leemos valor pot y mapeamos a n° de leds
  int valPot = analogRead(pinPot);
  valPot = map(valPot, 0, 1023, 0, 2);

  // encendemos Leds hasta valPot (escalado)
  for (int i = 0; i < valPot; i++) {
    digitalWrite(pinesLed[i], HIGH);
  }
  // apagamos el resto de los leds (sobre valor pot)
  for (int i = valPot; i < 2; i++) {
    digitalWrite(pinesLed[i], LOW);
  }
}
