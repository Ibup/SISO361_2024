% Template:     Informe LaTeX
% Documento:    Archivo principal
% Versión:      8.3.3 (07/03/2024)
% Codificación: UTF-8
%
% Autor: Pablo Pizarro R.
%        pablo@ppizarror.com
%
% Manual template: [https://latex.ppizarror.com/informe]
% Licencia MIT:    [https://opensource.org/licenses/MIT]

% CREACIÓN DEL DOCUMENTO
\documentclass[
	spanish, % Idioma: spanish, english, etc.
	letterpaper, oneside
]{article}

% PAQUETES
\usepackage{multirow}

% INFORMACIÓN DEL DOCUMENTO
\def\documenttitle {Nicolás Vásquez O.}
\def\documentsubtitle {}
\def\documentsubject {Tema a tratar}

\def\documentauthor {Nicolás Vásquez}
\def\coursename {Taller de Banda Sonoraa}
\def\coursecode {TBSO361}

\def\universityname {Universidad de Chile}
\def\universityfaculty {Facultad de Ciencias Físicas y Matemáticas}
\def\universitydepartment {Departamento de la Universidad}
\def\universitydepartmentimage {departamentos/fcfm}
\def\universitydepartmentimagecfg {height=1.57cm}
\def\universitylocation {Santiago de Chile}

% INTEGRANTES, PROFESORES Y FECHAS
\def\authortable {
	\begin{tabular}{ll}
		Integrantes:
		& \begin{tabular}[t]{l}
			Integrante 1 \\
			Integrante 2
		\end{tabular} \\
		Profesor:
		& \begin{tabular}[t]{l}
			Profesor 1
		\end{tabular} \\
		Auxiliar:
		& \begin{tabular}[t]{l}
			Auxiliar 1
		\end{tabular} \\
		Ayudantes:
		& \begin{tabular}[t]{l}
			Ayudante 1 \\
			Ayudante 2
		\end{tabular} \\
		\multicolumn{2}{l}{Ayudante de laboratorio: Ayudante 1} \\
		& \\
		\multicolumn{2}{l}{Fecha de realización: \today} \\
		\multicolumn{2}{l}{Fecha de entrega: \today} \\
		\multicolumn{2}{l}{\universitylocation}
	\end{tabular}
}

% IMPORTACIÓN DEL TEMPLATE
\input{template}

% INICIO DE PÁGINAS
\begin{document}

% PORTADA
%\templatePortrait

% CONFIGURACIÓN DE PÁGINA Y ENCABEZADOS
\templatePagecfg

% RESUMEN O ABSTRACT

% TABLA DE CONTENIDOS - ÍNDICE
%\templateIndex

% CONFIGURACIONES FINALES
\templateFinalcfg

% ======================= INICIO DEL DOCUMENTO =======================

\sectionanum{\centering Tarea 1: Diseño de interacciones}

Los diseños aquí presentados están relacionados: todos los objetos que los contienen se toman con la mano. Por dicha razón, la figura \ref{mano} a continuación muestra un esquema de las mediciones de mi mano que serán de utilidad para la valorización de algunos de estos diseños. 

\begin{images}[\label{mano}]{Estudio de la mano}
	\addimage{pics/mano}{width=7cm}{Vista de la palma}
	\addimage{pics/mano_side}{width=7.12cm}{Vista de apoyo lateral}
\end{images}

A continuación, se expondrán y explorarán brevemente cada uno de los diseños valorados:

\subsectionanum{\centering Diseños placenteros}

\begin{images}[]{}
	\addimage{pics/trackpoint}{width=6cm}{TrackPoint ThinkPad X60}
	\addimage{pics/platon}{width=6.03cm}{\textit{La República}, Aguilar (1959)}
\end{images}

\subsubsection*{(a) TrackPoint Thinkpad X60}

\noindent La distancia entre el TrackPoint y la zona de los botones es de $7$ cm, que se encuentra dentro de la extensión de reposo entre el dedo medio y el pulgar, presentado en la figura \ref{mano}b. Cada uno de los botones tiene un ancho de $2$ cm, logrando una extensión de uso de $6$ cm que son cómodos al movimiento libre del pulgar. Así, la mano está en posición permanente de reposo en su uso, siendo el único movimiento necesario la flexión del pulgar para presionar los botones. Así, se admite una experiencia de control absolutamente cómoda para la anatomía de mi mano.\newline

\noindent Algunas apreciaciones mías que hacen de su experiencia de uso algo placentero: 

 \begin{itemize}[]
 	\item[1.] El TrackPoint funciona con presión: la interacción del usuario es la aplicación de una pequeña fuerza sobre él, que es sucedido por una retroalimentación (\textit{feedback}) inmediata en el movimiento del cursor en la pantalla. La velocidad de dicho movimiento depende de la cantidad de presión que se aplique sobre él, permitiendo una navegación sencilla y una buena calidad de la interacción; si es que en algún momento el cursor no puede ser ubicado en la pantalla, el usuario puede moverlo rápidamente y detectarlo con facilidad.
 	
 	\item[2.] El TrackPoint se posiciona estratégicamente en la zona baja del teclado, con tal que el usuario no tenga la necesidad de mover o estirar los dedos para alcanzar sus botones, asegurando una buena relación y mapeo (\textit{mapping}) con sus otras necesidades. Es particularmente cómodo de utilizar en entornos que requieren de mucho uso del teclado, como sucede en la implementación de gestores de ventanas de mosaico [tiling window managers], que es mi caso. El uso del TrackPoint me asegura un flujo de trabajo continuo, sin interrupciones al momento de escribir y utilizar atajos que he configurado para permitirme navegar entre las ventanas de comando.
 	
 	\item[3.] El TrackPoint es llamativo y genera un contraste que, en el caso de no tener un modelo mental (\textit{mental model}) del dispositivo, invita al usuario a experimentar con él, haciendo dicho descubrimiento algo trivial. Eventualmente, su forma, ubicación y las implicaciones discutidas en [2] son una señal  (\textit{signifier}) de la prestación (\textit{affordance}) del TrackPoint, en tanto permiten al sujeto realizar asociaciones entre la distribución de cada una de las partes con las funciones de la computadora; en otras palabras, es intuitivo de descubrir su comportamiento.
 \end{itemize}

\subsubsection*{(b) \textit{La República}, Aguilar (1959)}

\noindent El libro tiene, de manera aproximada, dimensiones de $14$ cm $\cross\hspace{0.1cm}9$ cm, y con 660 páginas, sólo un grosor de $1.2$ cm. Como la extensión más larga desde el eje del pisiforme hasta mis dedos es de $16.8$ cm, el diseño se ajusta perfectamente dentro de la palma sin ser demasiado pequeño.\newline

\noindent Algunas apreciaciones mías que hacen de su experiencia de uso algo placentero:

\begin{itemize}
	\item[1.] Aunque su prestación fundamental es aquella de \textit{ser leído}, el tamaño del objeto, junto a mi uso de bolsos pequeños y bolsillos adecuadamente grandes, hace que en la relación de mis actividades aparezca también la de \textit{ser transportado fácilmente} (que evidentemente no todos los libros tienen).
	
	\item[2.] El diseño es de mi gusto; no sólo en referencia a su empastado y cobertura exterior, sino que el uso de hoja scritta lo hacen suave al tacto: parte de mi aproximación al objeto es, así, estética.
	
	\item[3.] Tengo una conexión emocional al libro físico y su dimensión literaria, en tanto fue un regalo de mi bisabuelo datado del año $2016$. relación que tengo con el objeto y su racionalización están, así, valorizadas.
\end{itemize}

\begin{images}[]{Interior del libro}
	\addimage{pics/bookin1}{width=8cm}{}
	\addimage{pics/bookin2}{width=8cm}{}
\end{images}

\subsectionanum{\centering Diseños frustrantes}
\begin{images}[]{}
	\addimage{pics/lapiz}{width=6.03cm}{Grafito HP ALO Color triangular}
	\addimage{pics/tenedor}{width=6.03cm}{Tenedor de mango grueso}
\end{images}

\subsubsection*{(a) Grafito HP ALOColor triangular}

\noindent El lápiz tiene una extensión de $18$ cm de largo, y cada lado de la pirámide que se extiende hasta la punta tiene un tamaño de $0.5$ cm.\newline

\noindent La apreciación del porqué este diseño es frustrante al uso:

\begin{itemize}
	\item[1.] Para apoyar el lapiz, uso el nudillo dorsal a la articulación que une las falanges distales y medias del dedo anular y el dedo pulgar. Esto, sumado a la gran presión que ejerzo al sostenerlo, hace que el diseño triangular del lapiz sea absolutamente incómodo a la mano, provocándome dolor en dichas zonas al escribir durante tiempo prolongado. No se trata de una incompatibilidad con la anatomía de la mano, sino con el despliegue de la acción; es debido a mi postura y comportamiento que la relación no es provechosa.	
	 
\end{itemize}


\begin{images}[\label{escritura}]{Posición de escritura}
	\addimageanum{pics/hand}{width=6cm}{}
\end{images}


\subsubsection*{(b) Tenedor de mango rectangular grueso}

\noindent El tenedor tiene una extensión de $19$ cm, con un grosor de $0.5$ cm. La apreciación del porqué el objeto me entrega una experiencia frustrante:


\begin{itemize}
	\item[1.] Sostengo el tenedor de igual manera como con el lápiz. Naturalmente no se producen discordancias entre las prestaciones del objeto y las concepciones mentales que se tienen de él, pero la textura firme y fria del acero en conjunción a la geometría incómoda lo tornan incómodo de usar.
	
	\item[2.] El punto [1] no sólo ha sido notado por mí: es una molestia para todos los habitantes de mi casa, lo que ha valorizado el objeto de mala manera y ha deteriorado nuestra relación con él: tiene, por dicha razón, fama de indeseable.
\end{itemize}

% FIN DEL DOCUMENTO
\end{document}