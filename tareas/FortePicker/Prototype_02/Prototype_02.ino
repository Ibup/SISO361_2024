/* 
---------------------------------------------------------------------------------------------------
Prototipo 02: Recorrido de luz y selección de LED con botón. Tempo controlable con un potenciómetro.
---------------------------------------------------------------------------------------------------
Dispositivo que enciende LEDs uno a uno a un tiempo determinado por el usuario.
El usuario puede "seleccionar" un LED presionando el botón, lo que no detiene el recorrido de luces.

*/

// Definición de pins.
#define temPot A5 // Potenciómetro de Tempo.
#define pinSpk 5 // Salida de altavoz.
#define pinBtn 13 // Botón.
int pinLed[] = { 2, 3 }; // Arreglo de pinLed.

// Estado de los componentes:
bool ledLock[] = { LOW, LOW }; // El arreglo contiene información sobre si un LED ha sido "fijado": de ahí el nombre de variable "ledLock".
bool lastBtn = HIGH;


// ------------- SETUP -------------------
void setup() { 
  // Configuración de pins como salidas:
  for ( int i = 0; i < 2; i++ ) {
    pinMode(pinLed[i], OUTPUT);
  }
  
  // PULLUP del Botón: 
  pinMode(pinBtn, INPUT_PULLUP);
}


// ------------------ LOOP ----------------------
void loop () {

  // ------ POT TEMPO:
  int valTemPot = analogRead(temPot);  
  int tempo = map(valTemPot, 0, 1023, 100, 1000); // >60BPM.
  // ------ END POT TEMPO.

  // ------ LED LOCK:
  bool stateBtn = digitalRead(pinBtn);

  // Cambia el estado de ledLock en el indice [i], al presionar el botón. 
  for (int i = 0; i < 2; i++) {
    if (stateBtn == LOW) {
      ledLock[i] = !ledLock[i];
      delay(100);
    }
 }
  // ------ END LED LOCK.

  // ------ LED RUN:
  for (int i = 0; i < 2; i++) {

    // Si el LED en la posición [i] está fijado por el usuario, encenderlo siempre.
    if (ledLock[i] == HIGH) {
      digitalWrite(pinLed[i], HIGH);
      delay(tempo); 
    } 
    // Si no está fijado, ON y OFF.
    else {
        digitalWrite(pinLed[i], HIGH);
        delay(tempo);
        digitalWrite(pinLed[i], LOW);
    }
  }
  
  for (int i = 1; i >= 0; i--) { // VUELTA; ídem:
    if (ledLock[i] == HIGH) {
      digitalWrite(pinLed[i], HIGH);
      delay(tempo); 
    } else {
      digitalWrite(pinLed[i], HIGH);
      delay(tempo);
      digitalWrite(pinLed[i], LOW);
    }
  }
  // ------ END LED RUN.
}
