#include <Arduino.h>
#line 1 "/home/ibup/Documents/universidad/SISO361/tareas/FortePicker/FortePicker.ino"
// Secuencia una melodía, donde el usuario debe reconocer los temas del poema del éxtasis.

// Definición de potenciómetros
#define ledPot A0 // Control de tempo.
#define temPot A1 // Control de figura.
#define figPot A2 // Control de Led.

// Altavoz:
#define pinSpk 5

// Definición de pinsLed:
int pinLed[] = { 2, 3 };

// Notas (octatónica):
int octScale[] = { 262, 277, 311, 330, 370, 392, 440, 466, 524 };
int octIndex = 0;
int pitchOct = 0;
// Definición de variables:
int ledChoice;
int tempo;
int figure;

unsigned long previousMillis = 0;
bool ledState = false;

#line 26 "/home/ibup/Documents/universidad/SISO361/tareas/FortePicker/FortePicker.ino"
void setup();
#line 34 "/home/ibup/Documents/universidad/SISO361/tareas/FortePicker/FortePicker.ino"
void loop();
#line 26 "/home/ibup/Documents/universidad/SISO361/tareas/FortePicker/FortePicker.ino"
void setup() {
 
  // Configuración de pins como salidas:
  for ( int i = 0; i < 2; i++ ) {
      pinMode(pinLed[i], OUTPUT);
    }
}

void loop () {

  // Lectura de los valores de los potenciómetros:
  int valLedPot = analogRead(ledPot);
  int valTemPot = analogRead(temPot); 
  int valFigPot = analogRead(figPot);

  tempo = map(valTemPot, 0, 1023, 150, 500);
  figure = tempo/map(valFigPot, 0, 1023, 1, 8);
  ledChoice = map(valLedPot, 0, 1023, 0, 2);

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= (ledState ? figure : tempo)) {

    previousMillis = currentMillis;
    ledState = !ledState;

    for (int i = 0; i < 2; i++) {
      digitalWrite(pinLed[i], i == ledChoice && ledState);
      octIndex = random(8);
      pitchOct = random(2);
      if (ledChoice == 0) {
        tone(pinSpk, octScale[octIndex]*2, figure);
        delay(figure);
        noTone(pinSpk);
      }

      if (ledChoice == 1) {
        tone(pinSpk, octScale[octIndex]/2, figure);
        delay(figure);
        noTone(pinSpk);
      }
    }
  }
}

