/* 
-----------------------------------------------------------
Prototipo 01: "Selección" de LED con potenciómetro. 
-----------------------------------------------------------
Dispositivo sonoro que emite una frecuencia dependiendo del LED "escogido" por el usuario, a un tempo y figura rítmica controlable.

Objetivo: Sincronizar el sonido emitido con la luz de un LED determinado.
*/


// Definición de potenciómetros
#define ledPot A0 // Control de tempo.
#define temPot A1 // Control de figura.
#define figPot A2 // Control de Led.

// Altavoz:
#define pinSpk 5

// Definición de pinsLed:
int pinLed[] = { 2, 3 };

// Definición de variables:
int ledChoice;
int tempo;
int figure;

// Inicialización de tiempo:
unsigned long prevTime;

// Estado del LED (true, false):
bool ledState = false;

void setup() {
 
  // Configuración de pins como salidas:
  for ( int i = 0; i < 2; i++ ) {
      pinMode(pinLed[i], OUTPUT);
    }
}

void loop () {

  // Lectura de los valores de los potenciómetros:
  int valLedPot = analogRead(ledPot);
  int valTemPot = analogRead(temPot); 
  int valFigPot = analogRead(figPot);

  tempo = map(valTemPot, 0, 1023, 150, 500);
  figure = tempo/map(valFigPot, 0, 1023, 1, 8);
  ledChoice = map(valLedPot, 0, 1023, 0, 2);

  // Tiempo actual
  unsigned long actualTime = millis();

  if (actualTime - prevTime >= (ledState ? figure : tempo)) {

    previousMillis = actualTime;
    ledState = !ledState;

    for (int i = 0; i < 2; i++) {
      digitalWrite(pinLed[i], i == ledChoice && ledState);
      octIndex = random(8);
      pitchOct = random(2);
      if (ledChoice == 0) {
        tone(pinSpk, octScale[octIndex]*2, figure);
        delay(figure);
        noTone(pinSpk);
      }

      if (ledChoice == 1) {
        tone(pinSpk, octScale[octIndex]/2, figure);
        delay(figure);
        noTone(pinSpk);
      }
    }
  }
}
